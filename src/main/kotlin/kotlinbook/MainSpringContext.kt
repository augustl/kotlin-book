package kotlinbook

import com.zaxxer.hikari.HikariDataSource
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.routing.*
import io.ktor.util.pipeline.*
import kotliquery.queryOf
import kotliquery.sessionOf
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.FactoryBean
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.beans.factory.config.BeanDefinitionCustomizer
import org.springframework.beans.factory.config.RuntimeBeanReference
import org.springframework.context.support.StaticApplicationContext
import javax.sql.DataSource

private val log = LoggerFactory.getLogger("kotlinbook.MainSpringContext")

fun main() {
    log.debug("Starting application...")
    val env = System.getenv("KOTLINBOOK_ENV") ?: "local"

    log.debug("Application runs in the environment ${env}")
    val config = createAppConfig(env)

    println("Creating app context")
    val ctx = createApplicationContext(config)

    println("Getting data source")
    val dataSource = ctx.getBean("dataSource", DataSource::class.java)
    println(dataSource)
    println(sessionOf(dataSource).use { sess ->
        sess.list(queryOf("SELECT * FROM user_t"), ::mapFromRow)
    })

//    println("Getting foo")
//    println( ctx.getBean("foo"))

    embeddedServer(Netty, port = config.httpPort) {
        createKtorApplication(config, dataSource)

        routing {
            get("/spring_context_test", ctx.getBean("springContextTestHandler") as PipelineInterceptor<Unit, ApplicationCall>)
        }
    }.start(wait = true)
}

class Foo() {
    lateinit var bar: Bar

    fun zing() {
        println("Initializing foo :D ${bar.doIt()}")
    }
}

class Bar() {
    fun zing() {
        println("Initializing bar :O")
    }

    fun doIt() = "it from bar"
}

class MigratedDataSourceFactoryBean : FactoryBean<DataSource> {
    lateinit var unmigratedDataSource: DataSource
    override fun getObject() =
        unmigratedDataSource.also(::migrateDataSource)
    override fun getObjectType() = DataSource::class.java
    override fun isSingleton() = true
}

fun createApplicationContext(appConfig: WebappConfig) =
    StaticApplicationContext().apply {
        beanFactory.registerSingleton("appConfig", appConfig)

        registerBean(
            "unmigratedDataSource",
            HikariDataSource::class.java,
            BeanDefinitionCustomizer { bd ->
                bd.propertyValues.apply {
                    add("jdbcUrl", appConfig.dbUrl)
                    add("username", appConfig.dbUser)
                    add("password", appConfig.dbPassword)
                }
            }
        )

        registerBean(
            "dataSource",
            MigratedDataSourceFactoryBean::class.java,
            BeanDefinitionCustomizer { bd ->
                bd.propertyValues.apply {
                    add(
                        "unmigratedDataSource",
                        RuntimeBeanReference("unmigratedDataSource")
                    )
                }
            }
        )

        beanFactory.registerSingleton("springContextTestHandler", webResponse {
            TextWebResponse("Hello from Spring!")
        })

        registerBean(
            "foo",
            Foo::class.java,
            object : BeanDefinitionCustomizer {
                override fun customize(bd: BeanDefinition) {
                    bd.isLazyInit = true
                    bd.initMethodName = "zing"
                    bd.propertyValues.apply {
                        add("bar", RuntimeBeanReference("bar"))
                    }
                }
            }
        )

        registerBean(
            "bar",
            Bar::class.java,
            BeanDefinitionCustomizer { bd ->
                bd.isLazyInit = true
                bd.initMethodName = "zing"
            }
        )

//        registerBean(
//            "thingDoer",
//            ThingDoer::class.java,
//            BeanDefinitionCustomizer { bd ->
//                bd.initMethodName = "start"
//                bd.destroyMethodName = "shutdown"
//                bd.propertyValues.apply {
//                    add("dataSource", RuntimeBeanReference("dataSource"))
//                }
//            }
//        )

        refresh()
        registerShutdownHook()
    }

//class ThingDoer {
//    lateinit var dataSource: DataSource
//
//    fun start() {
//        log.debug("Starting ThingDoer")
//    }
//
//    fun doThing() {
//        dataSource.connection
//    }
//
//    fun shutdown() {
//        log.debug("Stopping ThingDoer")
//    }
//}