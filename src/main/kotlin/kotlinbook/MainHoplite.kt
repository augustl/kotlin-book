package kotlinbook

import com.sksamuel.hoplite.ConfigLoaderBuilder
import com.sksamuel.hoplite.addResourceSource
import com.sksamuel.hoplite.preprocessor.EnvOrSystemPropertyPreprocessor
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import org.slf4j.LoggerFactory

private val log = LoggerFactory.getLogger("kotlinbook.MainHoplite")

fun main() {
    log.debug("Starting application...")
    val env = System.getenv("KOTLINBOOK_ENV") ?: "local"
    log.debug("Application runs in the environment ${env}")
    val config = ConfigLoaderBuilder.default()
        .addResourceSource("/app.conf")
        .addResourceSource("/app-${env}.conf")
        .addPreprocessor(EnvOrSystemPropertyPreprocessor)
        .build()
        .loadConfigOrThrow<WebappConfig>()
    log.debug("Configuration loaded successfully: ${config}")

    val dataSource = createAndMigrateDataSource(config)

    embeddedServer(Netty, port = config.httpPort) {
        createKtorApplication(config, dataSource)
    }.start(wait = true)
}