import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.6.21"
    application
    id("com.github.johnrengelman.shadow") version "7.0.0"
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("io.ktor:ktor-server-core-jvm:2.1.1")
    implementation("io.ktor:ktor-server-netty-jvm:2.1.1")
    implementation("io.ktor:ktor-server-html-builder:2.1.1")
    implementation("io.ktor:ktor-server-status-pages:2.1.1")

    implementation("com.google.code.gson:gson:2.9.0")

    implementation("ch.qos.logback:logback-classic:1.4.0")
    implementation("org.slf4j:slf4j-api:2.0.0")

    implementation("com.typesafe:config:1.4.2")

    implementation("com.zaxxer:HikariCP:5.0.1")
    implementation("com.h2database:h2:2.1.214")
    implementation("org.flywaydb:flyway-core:9.3.0")
    implementation("com.github.seratch:kotliquery:1.9.0")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")
    implementation("io.ktor:ktor-client-core:2.1.1")
    implementation("io.ktor:ktor-client-cio:2.1.1")

    implementation("org.springframework.security:spring-security-web:5.7.3")
    implementation("org.springframework.security:spring-security-config:5.7.3")

    implementation("io.arrow-kt:arrow-fx-coroutines:1.1.2")
    implementation("io.arrow-kt:arrow-fx-stm:1.1.2")

    implementation("io.ktor:ktor-server-servlet:2.1.1")
    implementation("org.eclipse.jetty:jetty-server:9.4.48.v20220622")
    implementation("org.eclipse.jetty:jetty-servlet:9.4.48.v20220622")

    implementation("org.springframework:spring-context:5.3.22")

    implementation("at.favre.lib:bcrypt:0.9.0")
    implementation("io.ktor:ktor-server-auth:2.1.1")
    implementation("io.ktor:ktor-server-auth-jwt:2.1.1")
    implementation("io.ktor:ktor-server-sessions:2.1.1")
    implementation("io.ktor:ktor-server-auth-jwt-jvm:2.1.1")

    implementation("io.ktor:ktor-server-cors-jvm:2.1.1")

    implementation("com.amazonaws:aws-lambda-java-core:1.2.1")

    implementation("io.jooby:jooby:2.16.1")
    implementation("io.jooby:jooby-netty:2.16.1")

    implementation("com.sksamuel.hoplite:hoplite-core:2.6.3")
    implementation("com.sksamuel.hoplite:hoplite-hocon:2.6.3")


    testImplementation("org.spekframework.spek2:spek-dsl-jvm:2.0.19")
    testImplementation("org.spekframework.spek2:spek-runner-junit5:2.0.19")
    testImplementation(kotlin("test"))
}

application {
    mainClass.set("kotlinbook.MainKt")
    applicationDefaultJvmArgs = listOf("-Dio.ktor.development=true")
}

tasks.test {
    useJUnitPlatform {
        includeEngines("spek2")
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "11"
}
